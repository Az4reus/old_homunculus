import logging
from util.discord_util import reply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def init():
    pass

def handle(message, client):
    response = "Roles for {}:\n".format(message.author.name)
    
    for role in message.author.roles:
        # replace @s so we don't spam everyone with pings
        r_name = role.name.replace('@', '(at)')
        response += "{} - {}\n".format(role.id, r_name)    
        
    reply(client, message, response)
