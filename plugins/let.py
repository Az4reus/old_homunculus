import logging
import json
from util.discord_util import reply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

#
# CONFIG
# 
# Anyone in this list may use !let anywhere
let_admins =   [
                    "132723675546779648", # Az
                    "134276184526684160"  # Tyraez
               ]
# Anyone in one or more of these groups may use !let
let_roles =    [
                    "133996157276913664" # ping in Rufflicious Inquisition#general
               ]
# Anyone in these channels may use !let
let_channels = [
                    
               ]
strings_file   =    "strings.json"
# default strings for when strings_file cannot be found
strings = dict({
                    "!ping": "We're not doing this again.",
                    "!trivia": "Fuck you, $mention. Blame Fenrir."
              })
#
# /CONFIG
#

def init():
    global strings
    strings = load_strings()

def handle(message, client):
    global strings
    
    # Default module action
    if message.content.startswith('!let'):
        # check permissions
        if may_let(message):
            handle_let(message, client)
        else:
            reply(client, message, "Access denied.")
    
    # The special poop command
    elif message.content.startswith('!poop'):
        if message.author.name == 'Ipoopedbad Ernaga':
            post = "Narcissist"		
        else:		
            post = "{} and Poop are friends.".format(message.author.mention())
        reply(client, message, post)
    
    # Execute existing custom command
    else:
        for cmd, string in strings.items():
            if message.content.startswith(cmd):
                post = parse(message, client, string)
                reply(client, message, post)
                
def handle_let(message, client):
        global strings            

        args = message.content[4:].strip()
        
        # Register new custom command        
        if '->' in args:
            args = args.split("->", 1)
            new_custom_cmd = args[0].strip()
            
            if len(new_custom_cmd) < 1:
                post = "Missing command name for new custom command."
            else:
                # Prepend '!' if necessary
                if not new_custom_cmd.startswith('!'):
                    new_custom_cmd  = '!' + new_custom_cmd
            
                    if len(args) < 2 or len(args[1]) < 1:
                        post = "Missing command body for new custom command."
                    else:
                        register_command(new_custom_cmd, args[1].strip())
                        post = "Registered custom command: " + new_custom_cmd
            
        # Delete existing custom command
        elif args.startswith('delete'):
            args = args.split(" ")
            if len(args) < 2:
                post = "Missing an argument."
            else:
                custom_cmd = args[1]
                
                # Prepend ! if necessary
                if not custom_cmd.startswith('!'):
                    custom_cmd = '!' + custom_cmd
                
                if custom_cmd in strings.keys():
                    delete_command(custom_cmd)
                    post = "Deleted custom command: " + custom_cmd
                else:
                    post = "Custom command '{}' not found.".format(custom_cmd)
                    
        # List custom commands
        elif args.startswith('list'):
            post = ''
            for custom_cmd, string in strings.items():
                post += "{} -> {}\n".format(custom_cmd, string)
                    
        # Unknown subcommand
        else:
            post = "Unknown subcommand."
            logger.info("Rejected command: " + message.content)  
        
        # Respond    
        reply(client, message, post) 
        
def register_command(cmd, body):
    global strings
    
    strings[cmd] = body
    logger.info("Registered command: " + cmd)
    persist_strings(strings) 
    
def delete_command(cmd):
    global strings
    
    del strings[cmd]
    logger.info("Deleted command: " + cmd)
    persist_strings(strings)
        
def parse(message, client, string):
    string = string.replace("$mention", message.author.mention())
    # add more replacements here
    
    return string

def load_strings() -> dict:
    global strings
    
    try:
        with open(strings_file) as fp:
            new_strings = json.load(fp).copy()
    except FileNotFoundError:
        new_strings = strings
        logger.warning("{} not found, using defaults".format(strings_file))
        
    return new_strings
    
def persist_strings(strings: dict):
    with open(strings_file, 'w') as fp:
        json.dump(strings, fp)
    logger.info("Custom commands saved to " + strings_file)
            
def may_let(message) -> bool:
    if message.author.id in let_admins:
        return True
        
    if message.channel.id in let_channels:
        return True
        
    for author_role in message.author.roles:
        if author_role.id in let_roles:
            return True
            
    return False
            
