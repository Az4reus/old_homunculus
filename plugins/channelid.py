import logging
from util.discord_util import reply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def init():
    pass


def handle(message, client):
    reply(client, message, "This channel's ID is **{}**".format(message.channel.id))
