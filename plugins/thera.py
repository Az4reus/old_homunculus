import logging
import functools
from api import evescout
from util.discord_util import reply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def handle(message, client):
    args = message.content[7:]
    
    thera_dist = evescout.query_thera_distance(args)
    
    jumps = 0
    
    try:
        for thera_hole in thera_dist:
            if jumps == 0 or thera_hole['jumps'] < jumps:
                jumps = thera_hole['jumps']
    
        reply(client, message, "The closest hole to Thera is **{}** jumps from {}".format(jumps, args))
    except KeyError:
        reply(client, message, "Invalid solar system")

def init():
    pass
