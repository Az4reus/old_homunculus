import sys
import logging
import discord
from util.discord_util import reply, log_in_client
from util.modules_util import *


modules = dict()
# Configure what to load here.
load_modules = ["price", "who", "let", "channelid", "roles", "thera"]

admins = [
            "132723675546779648", # Az
            "134276184526684160"  # Tyraez
         ]
         
logging.basicConfig(format='[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s',
                    level=logging.WARNING,
                    handlers=[logging.FileHandler("homunculus.log"),
                              logging.StreamHandler()])
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

client = discord.Client()

@client.event
def on_message(message):
    if message.author == client.user:
        return
        
    #
    # Global commands
    #

    if message.content.startswith('Are you there?'):
        reply(client, message, "Yes, yes I am. No worries. Everything is fine.")

    # elif message.content.startswith('!help'):
    #     for _, m in modules.items():
    #         m.help()
            
    elif message.content.startswith('!load'):
        if message.author.id in admins:
            handle_load_module(message)
        else:
            reply(client, message, "Access denied.")
                
    elif message.content.startswith('!unload'):
        if message.author.id in admins:
            handle_unload_module(message)
        else:
            reply(client, message, "Access denied.")
        
    #
    # Module commands
    #
    
    else:
    
        # 1 Command = 1 Module
        # Modules have priority over custom commands
        handled_by_module = False
        for c, m in modules.items():
            if message.content.startswith('!' + c):
                m.handle(message, client)
                handled_by_module = True
    
        # Fallthrough for custom commands handled by the 'let' module
        if not handled_by_module and is_loaded(modules, 'let') and message.content.startswith('!'):
            modules['let'].handle(message, client)


@client.event
def on_ready():
    for m in load_modules:
        load_module(modules, m)
    
    logger.info('Connected!')
    logger.info('Username: %s', client.user.name)
    logger.info('ID: %s', client.user.id)

def handle_load_module(message):
    module_name = message.content[6:].strip()
    
    try:
        load_module(modules, module_name)
        reply(client, message, "(Re)Loaded module '{}'".format(module_name))
        logger.info("(Re)Loaded module '%s'", module_name)
    except ImportError as e:
        reply(client, message, "Module hotload failed: " + str(e))
        logger.error("Module '%s' hotload failed: %s", module_name, repr(e))
    except ModuleInitError as e:
        reply(client, message, "Module '{}' failed to initialize".format(module_name))
        logger.error("Module '%s' failed to initialize: %s", module_name, repr(e))
    except Exception as e:
        reply(client, message, "Unexpected error during module hotload: " + repr(e))
        logger.error("Module '%s' hotload encountered an unexpected error: %s",
                      module_name, str(e))

def handle_unload_module(message):
    module_name = message.content[8:].strip()

    try:
        unload_module(modules, module_name)
        logger.info("Module '%s' unloaded", module_name)
        reply(client, message, "Module '{}' unloaded".format(module_name))
    except ModuleNotLoadedError:
        reply(client, message, "Module '{}' not present".format(module_name))
        logger.error("Module '%s' not loaded", module_name)
    except Exception as e:
        reply(client, message, "Module '{}' failed to unload".format(module_name))
        logger.error("Module '%s' failed to unload: %s", module_name, repr(e))

def main():
    """Main entry point"""
    log_in_client(client)
    client.run()
    
    return 0

if __name__ == '__main__':
    sys.exit(main())
