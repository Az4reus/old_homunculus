import importlib
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def load_module(modules: dict, module_name: str):
    module_full_name = 'plugins.' + module_name
    
    is_reload = module_name in modules.keys()
    
    if not is_reload:
        new_module = importlib.import_module(module_full_name)
        logger.info("Module '%s' loaded", module_name)
    else:
        new_module = importlib.reload(modules[module_name])
        del modules[module_name]
        # make sure the new version will be used
        importlib.invalidate_caches()
        logger.info("Module '%s' reloaded", module_name)
        
    modules[module_name] = new_module
    
    try:
        new_module.init()
        logger.info("Module '%s' initialized", module_name)
    except ModuleInitError as e:
        raise e
    except Exception as e:
        raise ModuleInitError(e)
        
def unload_module(modules: dict, module_name: str):
    if not module_name in modules.keys():
        raise ModuleNotLoadedError("Module '{}' is not loaded".format(module_name))
    
    # Python does not support unloading of modules, so this is all we can do
    del modules[module_name]
    
def is_loaded(modules, module_name: str) -> bool:
    return module_name in modules.keys()
    

class ModuleInitError(Exception):
    pass
    
class ModuleNotLoadedError(Exception):
    pass
