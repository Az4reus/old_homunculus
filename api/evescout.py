import requests

EVESCOUT_API_ROOT = 'https://eve-scout.com/api'


def system_query_url(system: str) -> str:
    return "{}/wormholes?systemSearch={}".format(EVESCOUT_API_ROOT, system)


def fetch_json_api(url: str) -> dict:
    return requests.get(url).json()


# High-level access.

def query_thera_distance(system: str) -> dict:
    return fetch_json_api(system_query_url(system))
